variable "region" {
  default = "us-west-2"
}

variable "accesskey" {}

variable "secretkey" {}

variable "windows-2012R2-amis" {
	type = "map"

	default = {
		us-west-2 = "ami-a8ca3dc8"
	}
}
