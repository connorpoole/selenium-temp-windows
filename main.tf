provider "aws" {
  access_key = "${var.accesskey}"
  secret_key = "${var.secretkey}"
  region = "${var.region}"
}

resource "terraform_remote_state" "subnets" {
  backend = "s3"
  config {
    bucket = "daqri-la"
    key = "terraform-remote-states/base_modules/subnets"
    region = "us-west-2"
  }
}

resource "terraform_remote_state" "security-groups" {
  backend = "s3"
  config {
    bucket = "daqri-la"
    key = "terraform-remote-states/base_modules/security-groups"
    region = "us-west-2"
  }
}

resource "terraform_remote_state" "keyPair" {
  backend = "s3"
  config {
    bucket = "daqri-la"
    key = "terraform-remote-states/base_modules/keypairs"
    region = "us-west-2"
  }
}

resource "aws_instance" "seleniumGridStandalone" {
  ami = "${lookup(var.windows-2012R2-amis, var.region)}"
  instance_type = "m4.large"
  subnet_id = "${terraform_remote_state.subnets.output.DAQRI-LA-2A_id}"
  vpc_security_group_ids = ["${terraform_remote_state.security-groups.output.windows_self_rdp_sgID}","${terraform_remote_state.security-groups.output.allow_all_from_daqri}"]
  key_name = "${terraform_remote_state.keyPair.output.key_name}"
  tags {
    Name = "seleniumWindows"
  }
}
